import argparse
from brightness_changer import input_check, brightness

parser = argparse.ArgumentParser()

parser.add_argument('color_bright', nargs='?', default=False)
args = parser.parse_args()


def main():
    """
    Обработка выбранного пользователем действия.
    Обрабатывывает введённое пользователем через командную строку действие,
    проверяя его на идентичность с True.
    """
    user_inp = input_check.raw_cb(args.color_bright)
    if user_inp is not None:
        mode, color, bright = user_inp
        match mode:
            case 'd':
                result = brightness.darken(color, bright)
                print(result)
            case 'l':
                result = brightness.lighten(color, bright)
                print(result)
            case _:
                print('Такого режима нет')
    else:
        print(
            "Help menu:",
            "python __main__.py 'd/l(darken/lighten) #RRGGBB(color) "
            "0-100(brightness)",
            sep="\n"
            )


if __name__ == '__main__':
    main()
