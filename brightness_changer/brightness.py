def darken(color, percent):
    """
    Затемнение цвета.
    """
    result = ''
    bright = 1 - percent / 100
    r = int(color[0] + color[1], 16)
    g = int(color[2] + color[3], 16)
    b = int(color[4] + color[5], 16)
    r = hex(round(r * bright))[2:]
    g = hex(round(g * bright))[2:]
    b = hex(round(b * bright))[2:]
    for i in r, g, b:
        if len(i) == 1:
            i = '0' + i
        result += i.upper()
    return result


def lighten(color, percent):
    """
    Осветление цвета.
    """
    result = ''
    bright = 1 + percent / 100
    r = int(color[0] + color[1], 16)
    g = int(color[2] + color[3], 16)
    b = int(color[4] + color[5], 16)
    if r == 0:
        r = hex(round(255*(bright-1)))[2:]
    else:
        r = hex(round(r * bright))[2:]
    if g == 0:
        g = hex(round(255*(bright-1)))[2:]
    else:
        g = hex(round(g * bright))[2:]
    if b == 0:
        b = hex(round(255*(bright-1)))[2:]
    else:
        b = hex(round(b * bright))[2:]
    for i in r, g, b:
        if len(i) == 1:
            i = '0' + i
        if len(i) > 2:
            i = 'ff'
        result += i.upper()
    return result
